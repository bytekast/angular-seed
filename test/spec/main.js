// This file is test/spec/main.js
var tests = Object.keys(window.__karma__.files).filter(function (file) {
  // run tests - only files ending with "Spec.js"
  return /Spec\.js$/.test(file);
});

requirejs.config({
  // Karma serves files from '/base'
  baseUrl: '/base/app/scripts',

  paths: {
    'angular-cookies': '../bower_components/angular-cookies/angular-cookies',
    'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
    'angular-resource': '../bower_components/angular-resource/angular-resource',
    'angular-sanitize': '../bower_components/angular-sanitize/angular-sanitize',
    'angular-scenario': '../bower_components/angular-scenario/angular-scenario',
    angular: '../bower_components/angular/angular',
    json3: '../bower_components/json3/build',
    'requirejs-domready': '../bower_components/requirejs-domready/domReady',
    'es5-shim': '../bower_components/es5-shim/es5-shim',
    requirejs: '../bower_components/requirejs/require',
    jquery: '../bower_components/jquery/jquery',

    unitTest: '../../test/spec'
  },

  shim: {
    angular: {
      exports: 'angular',
      deps: ['jquery']
    },
    angularMocks: { deps:['angular']}
  },

  // ask Require.js to load these files (all our tests)
  deps: tests,

  // start test run, once Require.js is done
  callback: window.__karma__.start
});