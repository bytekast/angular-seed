// main module for all services
define(['angular'], function(angular) {
   'use strict';
   return angular.module('services', []);
});