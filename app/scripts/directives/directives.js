// main module for all directives
define(['angular'], function(angular) {
   'use strict';
   return angular.module('directives', []);
});