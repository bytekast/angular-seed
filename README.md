pentaho-angular-seed
====================

Instructions:

1. Install http://yeoman.io
2. git clone git@github.com:bytekast/pentaho-angular-seed.git
3. cd pentaho-angular-seed
4. bower install
5. npm install

To run the app --> grunt server

To run the tests --> grunt test

To build a dist --> grunt